Отправка кода по СМС
====================

.. http:post:: /send-sms-code

   **Пример запроса**:

   .. sourcecode:: http

      POST /send-sms-code  HTTP/1.1
      Accept: application/json
      Content-Type: application/json

      {
         "phone": "77012223344",
         "is_test": true
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "auth_log_id": 1
      }

   :<json string, required phone: Номер телефона сотрудника
   :<json boolean, nullable is_test: Отправляется true для тестового режима
   :>json integer auth_log_id: Номер СМС отправки
   :statuscode 200: Успешный ответ
   :statuscode 422: Ошибка валидации
   :statuscode 400: Стандартная ошибка
