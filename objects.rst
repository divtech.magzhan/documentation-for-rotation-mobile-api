Объекты
=======

.. _employee_small_info:

Короткая информация о сотруднике
--------------------------------

.. code-block:: json

   {
      "id": 1,
      "full_name": "Тестов Тест",
      "phone": "77012223344",
      "status": "active",
      "position": "Full-stack engineer"
   }

.. list-table:: Описание
   :widths: 20 20 20 40
   :header-rows: 1

   * - Поле
     - Тип
     - Обязателен
     - Описание
   * - id
     - integer
     - да
     - ID сотрудника
   * - full_name
     - string
     - да
     - ФИО сотрудника
   * - phone
     - string
     -
     - Номер телефона сотрудника
   * - status
     - string
     - да
     - Статус сотрудника. Один из ``active``, ``dismissed``
   * - position
     - string
     - да
     - Должность сотрудника

.. _employee_full_info:

Полная информация по сотруднику
-------------------------------

.. code-block:: json

   {
      "id": 1,
      "full_name": "Жандосов Азамат",
      "first_name": "Азамат",
      "last_name": "Жандосов",
      "patronymic": null,
      "first_name_en": "Azamat",
      "last_name_en": "Zhandosov",
      "birth_date": "1990-01-01",
      "gender": "male",
      "country_code": "KAZ",
      "iin": "900101300606",
      "number": "010101",
      "position": "Software engineer",
      "phone": "77051112233",
      "additional_phone": null,
      "email": "azamat@gmail.com",
      "documents": [
         {
            "id": 80818,
            "type": "id-card",
            "number": "321321321",
            "issue_date": "2015-01-01",
            "expire_date": "2025-01-01",
            "issue_by": "МЮ"
         }
      ]
   }

.. list-table:: Описание
   :widths: 20 20 20 40
   :header-rows: 1

   * - Поле
     - Тип
     - Обязателен
     - Описание
   * - id
     - integer
     - да
     - ID сотрудника
   * - full_name
     - string
     - да
     - ФИО
   * - first_name
     - string
     - да
     - Имя
   * - last_name
     - string
     - да
     - Фамилия
   * - patronymic
     - string
     -
     - Отчество
   * - first_name_en
     - string
     -
     - Имя на англ.
   * - last_name_en
     - string
     -
     - Фамилия на англ.
   * - birth_date
     - string, date
     - да
     - Дата рождения
   * - gender
     - string
     - да
     - Пол. Один из ``male``, ``female``
   * - country_code
     - string
     - да
     - 3-х значный код гражданства
   * - iin
     - string
     - да
     - ИИН
   * - number
     - string
     - да
     - Таб. номер
   * - position
     - string
     - да
     - Должность
   * - phone
     - string
     -
     - Номер телефона
   * - additional_phone
     - string
     -
     - Доп. номер телефона
   * - email
     - string
     -
     - Email
   * - documents
     - array
     - да
     - Список объектов ":ref:`Документ сотрудника<emp_doc>`"

.. _emp_doc:

Документ сотрудника
-------------------

.. code-block:: json

   {
      "id": 80818,
      "type": "id-card",
      "number": "321321321",
      "issue_date": "2015-01-01",
      "expire_date": "2025-01-01",
      "issue_by": "МЮ"
   }

.. list-table:: Описание
   :widths: 20 20 20 40
   :header-rows: 1

   * - Поле
     - Тип
     - Обязателен
     - Описание
   * - id
     - integer
     - да
     - ID
   * - type
     - string
     - да
     - Тип. Один из ``id-card``, ``passport``, ``residence``, ``foreign``
   * - number
     - string
     - да
     - Номер документа
   * - issue_date
     - string, date
     -
     - Дата выписки
   * - expire_date
     - string, date
     -
     - Дата окончания
   * - issue_by
     - string
     -
     - Орган выдачи

.. _organization:

Организация
-----------

.. code-block:: json

   {
      "name": "TOO 'Тестовая организация'"
   }

.. list-table:: Описание
   :widths: 20 20 20 40
   :header-rows: 1

   * - Поле
     - Тип
     - Обязателен
     - Описание
   * - name
     - string
     - да
     - Название

.. _application:

Заявка
------

.. code-block:: json

   {
      "id": 19,
      "is_extra": false,
      "start_station": {
         "code": "2700770",
         "name": "ШЫМКЕНТ"
      },
      "end_station": {
         "code": "2700670",
         "name": "АКТОГАЙ"
      },
      "direction": "to-work",
      "shift": "day",
      "date": "2023-05-01",
      "status": "issued",
      "created_at": "2023-04-20T09:01:22.000000Z",
      "updated_at": "2023-04-20T09:04:19.000000Z",
      "issued_at": "2023-04-20T09:04:19.000000Z",
      "with_bus_transfer": false,
      "only_bus_transfer": false,
      "segments": [
         {
            "id": 28,
            "dep_station_code": "2700770",
            "dep_station_name": "ШЫМКЕНТ",
            "arr_station_code": "2700000",
            "arr_station_name": "АЛМАТЫ",
            "status": "issued",
            "active_process": null,
            "closed_reason": null,
            "created_at": "2023-04-20T09:02:03.000000Z",
            "updated_at": "2023-04-20T09:04:17.000000Z",
            "is_bus_transfer": false,
            "train": {
               "dep_station_code": "2700770",
               "dep_station_name": "ШЫМКЕНТ",
               "arr_station_code": "2700002",
               "arr_station_name": "АЛМАТЫ 1",
               "number": "034Т",
               "display_number": "034Т",
               "is_talgo": false,
               "dep_date_time": "2023-04-28T23:56:00.000000Z",
               "arr_date_time": "2023-04-29T13:40:00.000000Z",
               "with_el_reg": true,
               "in_way_minutes": 824,
               "created_at": "2023-04-20T09:02:03.000000Z",
               "updated_at": "2023-04-20T09:02:03.000000Z"
            },
            "ticket": {
               "id": 8851,
               "express_id": "72981148661415",
               "dep_station_code": "2700770",
               "dep_station_name": "ШЫМКЕНТ",
               "arr_station_code": "2700002",
               "arr_station_name": "АЛМАТЫ 1",
               "train_number": "034Т",
               "train_display_number": "034ТА",
               "is_talgo_train": 0,
               "dep_date_time": "2023-04-28T23:56:00.000000Z",
               "arr_date_time": "2023-04-29T13:40:00.000000Z",
               "car_type_label": "Плацкарт",
               "car_number": "13",
               "car_class": "3П",
               "seat_number": "019",
               "carrier_name": "АО ПАССАЖИРСКИЕ ПЕР-КИ",
               "sum": 5556,
               "return_sum": null,
               "return_commission": null,
               "returned_at": null,
               "status": "issued",
               "booked_at": "2023-04-20T09:03:22.000000Z",
               "issued_at": "2023-04-20T09:04:17.000000Z",
               "ticket_url": "http://rotation.test/download-print/8851/issue"
            }
         }
      ],
      "refund_applications": [
         {
            "id": 27,
            "employee_id": 47456,
            "application_id": 19,
            "reason": "test",
            "status": "pending",
            "user_id": null,
            "reject_reason": null,
            "created_at": "2023-04-28 10:15:00",
            "updated_at": "2023-04-28 10:15:00",
            "segments": [
               {
                  "id": 31,
                  "refund_application_id": 27,
                  "segment_id": 28,
                  "status": null,
                  "error_message": null,
                  "created_at": "2023-04-28 10:15:00",
                  "updated_at": "2023-04-28 10:15:00"
               }
            ]
         }
      ]
   }

.. list-table:: Описание
   :widths: 20 20 20 40
   :header-rows: 1

   * - Поле
     - Тип
     - Обязателен
     - Описание
   * - id
     - integer
     - да
     - ID
   * - is_extra
     - boolean
     - да
     - Признак экстренной заявки
   * - start_station
     - object
     - да
     - Начальная станция
   * - end_station
     - object
     - да
     - Конечная станция
   * - direction
     - string
     -
     - Направление. Один из ``to-work``, ``to-home``
   * - shift
     - string
     -
     - Смена. Один из ``day``, ``night``
   * - date
     - string, date
     - да
     - Дата заявки
   * - status
     - string
     - да
     - Статус заявки. Один из: ``opened``, ``issued``, ``canceled``, ``partly``, ``returned``
   * - created_at
     - string, date
     - да
     - Дата создания
   * - updated_at
     - string, date
     - да
     - Дата обновления
   * - issued_at
     - string, date
     -
     - Дата оформления
   * - segments
     - array
     - да
     - Список сегментов

.. _segment:

Сегмент
-------

.. code-block:: json

   {
      "id": 28,
      "dep_station_code": "2700770",
      "dep_station_name": "ШЫМКЕНТ",
      "arr_station_code": "2700000",
      "arr_station_name": "АЛМАТЫ",
      "status": "issued",
      "active_process": null,
      "closed_reason": null,
      "created_at": "2023-04-20 15:02:03",
      "updated_at": "2023-04-20 15:04:17",
      "watcher_time_limit": "2023-04-30T03:00:00.000000Z",
      "watcher_action": "issue",
      "train": {
         "dep_station_code": "2700770",
         "dep_station_name": "ШЫМКЕНТ",
         "arr_station_code": "2700002",
         "arr_station_name": "АЛМАТЫ 1",
         "number": "034Т",
         "display_number": "034Т",
         "is_talgo": false,
         "dep_date_time": "2023-04-28T23:56:00.000000Z",
         "arr_date_time": "2023-04-29T13:40:00.000000Z",
         "with_el_reg": true,
         "in_way_minutes": 824,
         "created_at": "2023-04-20T09:02:03.000000Z",
         "updated_at": "2023-04-20T09:02:03.000000Z"
      },
      "ticket": {
         "id": 8851,
         "express_id": "72981148661415",
         "dep_station_code": "2700770",
         "dep_station_name": "ШЫМКЕНТ",
         "arr_station_code": "2700002",
         "arr_station_name": "АЛМАТЫ 1",
         "train_number": "034Т",
         "train_display_number": "034ТА",
         "is_talgo_train": 0,
         "dep_date_time": "2023-04-28T23:56:00.000000Z",
         "arr_date_time": "2023-04-29T13:40:00.000000Z",
         "car_type_label": "Плацкарт",
         "car_number": "13",
         "car_class": "3П",
         "seat_number": "019",
         "carrier_name": "АО ПАССАЖИРСКИЕ ПЕР-КИ",
         "sum": 5556,
         "return_sum": null,
         "return_commission": null,
         "returned_at": null,
         "status": "issued",
         "issued_at": "2023-04-20T09:04:17.000000Z"
      }
   }

.. list-table:: Описание
   :widths: 20 20 20 40
   :header-rows: 1

   * - Поле
     - Тип
     - Обязателен
     - Описание
   * - id
     - integer
     - да
     - ID
   * - dep_station_code
     - string
     -
     - Код станции отправления
   * - dep_station_name
     - string
     - да
     - Название станции отправления
   * - arr_station_code
     - string
     -
     - Код станции прибытия
   * - arr_station_name
     - string
     - да
     - Название станции прибытия
   * - status
     - string
     - да
     - Статус заявки. Один из: ``opened``, ``issued``, ``returned``, ``canceled``, ``not-used``, ``confirmed``
   * - active_process
     - string
     -
     - Активная операция. Один из: ``booking``, ``refund``, ``watching``
   * - closed_reason
     - string
     -
     - Причина отмены или возврата
   * - created_at
     - string, date
     - да
     - Дата создания
   * - updated_at
     - string, date
     - да
     - Дата обновления
   * - train
     - object
     -
     - Информация о поезде
   * - ticket
     - object
     -
     - Информация о билете
   * - watcher_time_limit
     - string, datetime
     -
     - Срок листа ожидания
   * - watcher_action
     - string
     -
     - Действия листа ожидания. Один из ``issue``, ``notify``

.. _ticket:

Билет
-----

.. code-block:: json

   {
      "id": 8851,
      "express_id": "72981148661415",
      "dep_station_code": "2700770",
      "dep_station_name": "ШЫМКЕНТ",
      "arr_station_code": "2700002",
      "arr_station_name": "АЛМАТЫ 1",
      "train_number": "034Т",
      "train_display_number": "034ТА",
      "is_talgo_train": false,
      "dep_date_time": "2023-04-28T23:56:00.000000Z",
      "arr_date_time": "2023-04-29T13:40:00.000000Z",
      "car_type_label": "Плацкарт",
      "car_number": "13",
      "car_class": "3П",
      "seat_number": "019",
      "carrier_name": "АО ПАССАЖИРСКИЕ ПЕР-КИ",
      "sum": 5556,
      "return_sum": null,
      "return_commission": null,
      "returned_at": null,
      "status": "issued",
      "issued_at": "2023-04-20T09:04:17.000000Z"
   }

.. list-table:: Описание
   :widths: 20 20 20 40
   :header-rows: 1

   * - Поле
     - Тип
     - Обязателен
     - Описание
   * - id
     - integer
     - да
     - ID
   * - express_id
     - string
     - да
     - Номер билета
   * - dep_station_code
     - string
     - да
     - Станция отправления (код)
   * - dep_station_name
     - string
     - да
     - Станция отправления (название)
   * - train_number
     - string
     - да
     - Номер поезда
   * - train_display_number
     - string
     - да
     - Номер поезда для отображения
   * - is_talgo_train
     - boolean
     - да
     - Признак тальго поезда
   * - dep_date_time
     - string, datetime
     - да
     - Дата отправления
   * - arr_date_time
     - string, datetime
     - да
     - Дата прибытия
   * - car_type_label
     - string
     - да
     - Тип вагона
   * - car_number
     - string
     - да
     - Номер вагона
   * - car_class
     - string
     - да
     - Класс вагона
   * - seat_number
     - string
     - да
     - Номер места
   * - carrier_name
     - string
     - да
     - Перевозчик
   * - sum
     - integer
     - да
     - Стоимость билета
   * - return_sum
     - integer
     -
     - Возвращенная сумма
   * - return_commission
     - integer
     -
     - Комиссия за возврат
   * - returned_at
     - string, datetime
     -
     - Дата и время возврата
   * - status
     - string
     - да
     - Один из ``booked``, ``issued``, ``returned``, ``canceled``
   * - issued_at
     - string, datetime
     -
     - Дата и время выписки


.. _refund_application:

Заявка на возврат
-----------------

.. code-block:: json

   {
      "id": 1,
      "reason": "test",
      "status": "pending",
      "reject_reason": null,
      "created_at": "2023-04-28T04:15:00.000000Z",
      "updated_at": "2023-04-28T04:15:00.000000Z",
      "segments": [
         {
            "id": 1,
            "segment_id": 28,
            "status": null,
            "error_message": null,
            "created_at": "2023-04-28T04:15:00.000000Z",
            "updated_at": "2023-04-28T04:15:00.000000Z"
         }
      ]
   }

.. list-table:: Описание
   :widths: 20 20 20 40
   :header-rows: 1

   * - Поле
     - Тип
     - Обязателен
     - Описание
   * - id
     - integer
     - да
     - ID
   * - reason
     - string
     - да
     - Причина возврата
   * - status
     - string
     - да
     - Один из ``pending``, ``process``, ``completed``, ``rejected``, ``canceled``, ``partly``, ``error``
   * - reject_reason
     - string
     -
     - Причина отказа в возврате
   * - created_at
     - string, datetime
     - да
     - Дата и время выписки
   * - updated_at
     - string, datetime
     - да
     - Дата и время выписки
   * - segments
     - array
     - да
     - Список сегментов для возврата
   * - segments.*.id
     - integer
     - да
     - ID
   * - segments.*.segment_id
     - integer
     - да
     - ID сегмента
   * - segments.*.status
     - string
     -
     - Один из ``canceled``, ``process``, ``returned``, ``error``
   * - segments.*.error_message
     - string
     -
     - Текст ошибки. Когда status === error
   * - segments.*.created_at
     - string, date
     - да
     - Дата создания
   * - segments.*.updated_at
     - string, date
     - да
     - Дата обновления
