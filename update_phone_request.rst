Запись нового номера телефона
=============================

.. http:post:: /update-phone-request

   **Пример запроса**:

   .. sourcecode:: http

      POST /update-phone-request  HTTP/1.1
      Accept: application/json
      Content-Type: application/json

      {
         "employee_id": 1,
         "phone": "77771112233",
         "firebase_token": "tokenvalue"
      }

   :<json integer, required employee_id: ID сотрудника
   :<json string, required phone: Номер телефона сотрудника
   :<json string, required firebase_token: Firebase токен
   :statuscode 200: Успешный ответ
