Создание заявки на возврат
===========================

.. http:post:: /refund-applications

   **Пример запроса**:

   .. sourcecode:: http

      POST /refund-applications  HTTP/1.1
      Accept: application/json
      Content-Type: application/json

      {
         "application_id": 19,
         "segments_id": [28],
         "reason": "Тестовое описание"
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "id": 28
      }

   :<json integer, required application_id: ID заявки
   :<json array, required segments_id: Список сегментов для возврата
   :<json integer, required segments_id.*: ID сегмента
   :<json string, required reason: Причина возврата
   :>json integer id: ID созданной заявки для возврата
   :statuscode 200: Успешный ответ
   :statuscode 422: Ошибка валидации
   :statuscode 400: Стандартная ошибка

