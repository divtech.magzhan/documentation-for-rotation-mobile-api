Обновление документа
====================

.. http:post:: /profile/update-document

   **Пример запроса**:

   .. sourcecode:: http

      POST /profile/update-document  HTTP/1.1
      Accept: application/json
      Content-Type: application/json

      {
         "type": "id-card",
         "number": "123412341234",
         "issue_date": "2020-01-01",
         "expire_date": null,
         "issue_by": null,
         "use_as_default": false
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK

   :<json string, required type: Тип документа (Один из id-card, passport, residence, foreign)
   :<json string, required, max 255 number: Номер документа
   :<json string, date issue_date: Дата выписки
   :<json string, date expire_date: Дата окончания
   :<json string issue_by: Орган выдачи
   :<json boolean, required use_as_default: Использовать по умолчанию
