Получение информации по одной заявке
=====================================


.. http:get:: /applications/(int: id)

   **Пример запроса**:

   .. sourcecode:: http

      GET /applications/19 HTTP/1.1
      Accept: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "data": {
            "id": 19,
            "is_extra": false,
            "start_station": {
               "code": "2700770",
               "name": "ШЫМКЕНТ"
            },
            "end_station": {
               "code": "2700670",
               "name": "АКТОГАЙ"
            },
            "direction": "to-work",
            "shift": "day",
            "date": "2023-05-01",
            "status": "issued",
            "created_at": "2023-04-20T09:01:22.000000Z",
            "updated_at": "2023-04-20T09:04:19.000000Z",
            "issued_at": "2023-04-20T09:04:19.000000Z",
            "with_bus_transfer": false,
            "only_bus_transfer": false,
            "segments": [
               {
                  "id": 28,
                  "dep_station_code": "2700770",
                  "dep_station_name": "ШЫМКЕНТ",
                  "arr_station_code": "2700000",
                  "arr_station_name": "АЛМАТЫ",
                  "status": "issued",
                  "active_process": null,
                  "closed_reason": null,
                  "created_at": "2023-04-20T09:02:03.000000Z",
                  "updated_at": "2023-04-20T09:04:17.000000Z",
                  "is_bus_transfer": false,
                  "watcher_time_limit": null,
                  "watcher_action": null,
                  "train": {
                     "dep_station_code": "2700770",
                     "dep_station_name": "ШЫМКЕНТ",
                     "arr_station_code": "2700002",
                     "arr_station_name": "АЛМАТЫ 1",
                     "number": "034Т",
                     "display_number": "034Т",
                     "is_talgo": false,
                     "dep_date_time": "2023-04-28T23:56:00.000000Z",
                     "arr_date_time": "2023-04-29T13:40:00.000000Z",
                     "with_el_reg": true,
                     "in_way_minutes": 824,
                     "created_at": "2023-04-20T09:02:03.000000Z",
                     "updated_at": "2023-04-20T09:02:03.000000Z"
                  },
                  "ticket": {
                     "id": 8851,
                     "express_id": "72981148661415",
                     "dep_station_code": "2700770",
                     "dep_station_name": "ШЫМКЕНТ",
                     "arr_station_code": "2700002",
                     "arr_station_name": "АЛМАТЫ 1",
                     "train_number": "034Т",
                     "train_display_number": "034ТА",
                     "is_talgo_train": false,
                     "dep_date_time": "2023-04-28T23:56:00.000000Z",
                     "arr_date_time": "2023-04-29T13:40:00.000000Z",
                     "car_type_label": "Плацкарт",
                     "car_number": "13",
                     "car_class": "3П",
                     "seat_number": "019",
                     "carrier_name": "АО ПАССАЖИРСКИЕ ПЕР-КИ",
                     "sum": 5556,
                     "return_sum": null,
                     "return_commission": null,
                     "returned_at": null,
                     "status": "issued",
                     "booked_at": "2023-04-20T09:03:22.000000Z",
                     "issued_at": "2023-04-20T09:04:17.000000Z",
                     "ticket_url": "http://rotation.test/download-print/8851/issue"
                  }
               },
               {
                  "id": 29,
                  "dep_station_code": "2700000",
                  "dep_station_name": "АЛМАТЫ",
                  "arr_station_code": "2700670",
                  "arr_station_name": "АКТОГАЙ",
                  "status": "issued",
                  "active_process": null,
                  "closed_reason": null,
                  "created_at": "2023-04-20T09:02:03.000000Z",
                  "updated_at": "2023-04-20T09:04:19.000000Z",
                  "is_bus_transfer": false,
                  "watcher_time_limit": null,
                  "watcher_action": null,
                  "train": {
                     "dep_station_code": "2700002",
                     "dep_station_name": "АЛМАТЫ 1",
                     "arr_station_code": "2700670",
                     "arr_station_name": "АКТОГАЙ",
                     "number": "351Ц",
                     "display_number": "352*Ц",
                     "is_talgo": false,
                     "dep_date_time": "2023-04-30T03:34:00.000000Z",
                     "arr_date_time": "2023-04-30T16:47:00.000000Z",
                     "with_el_reg": true,
                     "in_way_minutes": 793,
                     "created_at": "2023-04-20T09:02:03.000000Z",
                     "updated_at": "2023-04-20T09:02:03.000000Z"
                  },
                  "ticket": {
                     "id": 8852,
                     "express_id": "73031148661426",
                     "dep_station_code": "2700002",
                     "dep_station_name": "АЛМАТЫ 1",
                     "arr_station_code": "2700670",
                     "arr_station_name": "АКТОГАЙ",
                     "train_number": "351Ц",
                     "train_display_number": "352*ЦА",
                     "is_talgo_train": false,
                     "dep_date_time": "2023-04-30T03:34:00.000000Z",
                     "arr_date_time": "2023-04-30T16:47:00.000000Z",
                     "car_type_label": "Плацкарт",
                     "car_number": "01",
                     "car_class": "3П",
                     "seat_number": "005",
                     "carrier_name": "АО ПАССАЖИРСКИЕ ПЕР-КИ",
                     "sum": 4062,
                     "return_sum": null,
                     "return_commission": null,
                     "returned_at": null,
                     "status": "issued",
                     "booked_at": "2023-04-20T09:03:24.000000Z",
                     "issued_at": "2023-04-20T09:04:19.000000Z",
                     "ticket_url": "http://rotation.test/download-print/8852/issue"
                  }
               }
            ],
            "refund_applications": [
               {
                  "id": 27,
                  "reason": "test",
                  "status": "pending",
                  "reject_reason": null,
                  "created_at": "2023-04-28T04:15:00.000000Z",
                  "updated_at": "2023-04-28T04:15:00.000000Z",
                  "segments": [
                     {
                        "id": 31,
                        "segment_id": 28,
                        "status": null,
                        "error_message": null,
                        "created_at": "2023-04-28T04:15:00.000000Z",
                        "updated_at": "2023-04-28T04:15:00.000000Z"
                     }
                  ]
               }
            ]
         }
      }

   :>json object data: :ref:`Объект заявки <application>`
