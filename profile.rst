Профайл
=======

.. http:get:: /profile

   **Пример запроса**:

   .. sourcecode:: http

      GET /profile HTTP/1.1
      Accept: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "data": {
            "id": 1,
            "full_name": "Жандосов Азамат",
            "first_name": "Азамат",
            "last_name": "Жандосов",
            "patronymic": null,
            "first_name_en": "Azamat",
            "last_name_en": "Zhandosov",
            "birth_date": "1990-01-01",
            "gender": "male",
            "country_code": "KAZ",
            "iin": "900101300606",
            "number": "010101",
            "position": "Software engineer",
            "phone": "77051112233",
            "additional_phone": null,
            "email": "azamat@gmail.com",
            "documents": [
               {
                  "id": 80818,
                  "type": "id-card",
                  "number": "321321321",
                  "issue_date": "2015-01-01",
                  "expire_date": "2025-01-01",
                  "issue_by": "МЮ"
               }
            ]
         }
      }

   :>json object data: :ref:`Информация о сотруднике <employee_full_info>`
   :statuscode 200: Успешный ответ
   :statuscode 400: Стандартная ошибка
   :statuscode 422: Ошибка валидации
