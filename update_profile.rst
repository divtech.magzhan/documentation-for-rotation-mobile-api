Обновление профайла
===================

.. http:post:: /profile

   **Пример запроса**:

   .. sourcecode:: http

      POST /profile  HTTP/1.1
      Accept: application/json
      Content-Type: application/json

      {
         "first_name": "Азамат",
         "last_name": "Жандосов",
         "patronymic": null,
         "first_name_en": "Azamat",
         "last_name_en": "Zhandosov",
         "birth_date": "1990-01-01",
         "gender": "male",
         "country_code": "KAZ",
         "iin": "900101300101",
         "phone": "77771112233",
         "additional_phone": null,
         "email": null
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK

   :<json string, required, max 255 first_name: Имя
   :<json string, required, max 255 last_name: Фамилия
   :<json string, max 255 patronymic: Отчество
   :<json string, max 255 first_name_en: Имя на анг.
   :<json string, max 255 last_name_en: Фамилия на анг.
   :<json date, required birth_date: Дата рождения
   :<json string, required country_code: 3-х значный код страны
   :<json string, required iin: ИИН
   :<json string, required phone: Номер телефона
   :<json string additional_phone: Номер телефона
   :<json email email: Email

