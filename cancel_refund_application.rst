Отмена заявки на возврат
========================

.. http:post:: /refund-applications/(int:id)/cancel

   **Пример запроса**:

   .. sourcecode:: http

      POST /refund-applications/28/cancel  HTTP/1.1
      Accept: application/json
      Content-Type: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

   :statuscode 200: Успешный ответ
   :statuscode 422: Ошибка валидации
   :statuscode 400: Стандартная ошибка

