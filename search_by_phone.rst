Поиск сотрудника по номеру телефона
===================================

.. important::

    Данный запрос должен отправляться в proxy сервер

.. http:get:: /get-employee-by-phone

   **Пример запроса**:

   .. sourcecode:: http

      GET /get-employee-by-phone?phone=77771112233 HTTP/1.1
      HOST: proxy.odyssey.kz
      Accept: application/json

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "exists": true,
         "employee": {
            "id": 1,
            "full_name": "Тестов Тест",
            "phone": "77012223344",
            "status": "active",
            "position": "Full-stack engineer"
         }
      }

   :query string, required phone: Номер телефона без спецсимволов
   :>json boolean exists: Результат поиска
   :>json object employee: :ref:`Короткая информация о сотруднике <employee_small_info>`
   :statuscode 200: Успешный ответ
   :statuscode 400: Стандартная ошибка
   :statuscode 422: Ошибка валидации
