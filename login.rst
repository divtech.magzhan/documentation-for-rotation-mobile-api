Аутентификация
==============

.. http:post:: /login

   **Пример запроса**:

   .. sourcecode:: http

      POST /login  HTTP/1.1
      Accept: application/json
      Content-Type: application/json

      {
         "phone": "77771112233",
         "code": "1111",
         "auth_log_id": 1
      }

   **Пример ответа**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      {
         "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjBhOWU5M2Q0MGUwZmE1...",
         "employee": {
            "id": 1,
            "full_name": "Жандосов Азамат",
            "first_name": "Азамат",
            "last_name": "Жандосов",
            "patronymic": null,
            "first_name_en": "Azamat",
            "last_name_en": "Zhandosov",
            "birth_date": "1990-01-01",
            "gender": "male",
            "country_code": "KAZ",
            "iin": "900101300606",
            "number": "010101",
            "position": "Software engineer",
            "phone": "77051112233",
            "additional_phone": null,
            "email": "azamat@gmail.com",
            "documents": [
               {
                  "id": 80818,
                  "type": "id-card",
                  "number": "321321321",
                  "issue_date": "2015-01-01",
                  "expire_date": "2025-01-01",
                  "issue_by": "МЮ"
               }
            ]
         },
         "organization": {
            "name": "TOO 'Тестовая организация'"
         }
      }

   :<json string, required phone: Номер телефона сотрудника
   :<json string, required code: Код полученный по СМС
   :<json integer, required auth_log_id: Номер СМС отправки
   :>json string token: Токен аутентификации
   :>json object employee: :ref:`Полная информация о сотруднике<employee_full_info>`
   :>json object organization: :ref:`Информация об организации <organization>`
   :statuscode 200: Успешный ответ
   :statuscode 422: Ошибка валидации
   :statuscode 400: Стандартная ошибка
