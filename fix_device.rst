Запись firebase токена
======================

.. http:post:: /fix-device

   **Пример запроса**:

   .. sourcecode:: http

      POST /fix-device  HTTP/1.1
      Accept: application/json
      Content-Type: application/json

      {
         "token": "tokenvalue",
         "device_type": "Samsung",
         "device_os": "Android 12",
      }

   :<json string, required token: Firebase токен
   :<json string, nullable device_type: Тип устройства
   :<json string, nullable device_os: ОС устройства
   :statuscode 200: Успешный ответ
